package Day01_hw;

class Person{
	String firstName;
	String lastName;
	int age;
	public String getFirstName() {
		return this.firstName;
	}
	public String getLastName() {
		return this.lastName;
	}
	public int getAge() {
		return this.age;
	}
	public void setFirstName(String name) {
		this.firstName = name;
	}
	public void setLastName(String name) {
		this.lastName = name;
	}
	public void setAge(int age) {
		this.age = age;
		if ( age < 0 || age > 100) {
			this.age = 0;
		}
	}
	public boolean isTeen(){
		if ( this.age > 12 && this.age < 20) {
			return true;
		}else {
		return false;
		}
	}
	public String getFullName() {
		if( this.firstName.isEmpty() && this.lastName.isEmpty()) {
			return "";
		} else if( this.lastName.isEmpty()) {
			return this.firstName;
		} else if( this.firstName.isEmpty()) {
			return this.lastName;
		}else {
			return (this.firstName + " " + this.lastName);
		}
	}
}


//Sum Calculator

class SimpleCalculator{
	double firstNumber;
	double secondNumber;
	public double getFirstNumber() {
		return this.firstNumber;
	}
	public double getSecondNumber() {
		return this.secondNumber;
	}
	
	public void setFirstNumber(double number) {
		this.firstNumber = number;
	}
	public void setSecondNumber(double number) {
		this.secondNumber = number;
	}
	public double getAdditionResult() {
		return this.firstNumber + this.secondNumber;  
	}
	public double getSubstractionResult() {
		return this.firstNumber - this.secondNumber;  
	}
	public double getMultiplicationResult() {
		return this.firstNumber * this.secondNumber;  
	}
	public double getDivisionResult() {
		if (this.secondNumber == 0) {
			return 0;
		}
		return this.firstNumber / this.secondNumber;
	}
	
}

// Wall Area

class Wall{
	double width;
	double height;
	public Wall(){
		
	}
	public Wall(double width, double height){
		this.width = width;
		if( width < 0) {
			this.width = 0;
		}
		this.height = height;
		if( height < 0) {
			this.height = 0;
		}
	}
	public double getWidth() {
		return this.width;
	}
	public double getHeight() {
		return this.height;
	}
	
	public void setWidth(double width) {
		this.width = width;
		if( width < 0) {
			this.width = 0;
		}
	}
	public void setHeight(double height) {
		this.height = height;
		if( height < 0) {
			this.height = 0;
		}
	}
	public double getArea() {
		return this.width * this.height;
	}
	
}

public class Day01_hw_app {

	public static void main(String[] args) {
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("");
		person.setAge(10);
		System.out.println("fullName= " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setFirstName("John");
		person.setAge(18);
		System.out.println("fullName= " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setLastName("Smith");
		System.out.println("fullName= " + person.getFullName());
		System.out.println();
		
		//Sum Calculator
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("subtract= " + calculator.getSubstractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply= " + calculator.getMultiplicationResult());
		System.out.println("divide= " + calculator.getDivisionResult());
		System.out.println();
		
		// Wall Area
		Wall wall = new Wall(5,4);
		System.out.println("area= " + wall.getArea());
		wall.setHeight(-1.5);
		System.out.println("width= " + wall.getWidth());
		System.out.println("height= " + wall.getHeight());
		System.out.println("area= " + wall.getArea());
	}
}
